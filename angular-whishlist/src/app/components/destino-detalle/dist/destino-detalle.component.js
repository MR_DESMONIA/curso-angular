"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.DestinoDetalleComponent = void 0;
var core_1 = require("@angular/core");
var destinos_api_client_model_1 = require("./../../models/destinos-api-client.model");
var DestinoDetalleComponent = /** @class */ (function () {
    function DestinoDetalleComponent(route, destinosApiClient) {
        this.route = route;
        this.destinosApiClient = destinosApiClient;
        this.style = {
            sources: {
                world: {
                    type: 'geojson',
                    data: 'https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json'
                }
            },
            version: 8,
            layers: [{
                    'id': 'countries',
                    'type': 'fill',
                    'source': 'world',
                    'layout': {},
                    'paint': {
                        'fill-color': '#6F788A'
                    }
                }]
        };
    }
    DestinoDetalleComponent.prototype.ngOnInit = function () {
        var id = this.route.snapshot.paramMap.get('id');
        this.destino = this.destinosApiClient.getById(id);
    };
    DestinoDetalleComponent = __decorate([
        core_1.Component({
            selector: 'app-destino-detalle',
            templateUrl: './destino-detalle.component.html',
            styleUrls: ['./destino-detalle.component.css'],
            providers: [destinos_api_client_model_1.DestinosApiClient]
        })
    ], DestinoDetalleComponent);
    return DestinoDetalleComponent;
}());
exports.DestinoDetalleComponent = DestinoDetalleComponent;
