"use strict";
exports.__esModule = true;
var destinos_viajes_state_model_1 = require("./destinos-viajes-state.model");
var destino_viaje_model_1 = require("./destino-viaje.model");
describe('reducerDestinosViajes', function () {
    it('should reduce init data', function () {
        //set up
        var prevState = destinos_viajes_state_model_1.intializeDestinosViajesState();
        var action = new destinos_viajes_state_model_1.InitMyDataAction(['destino 1', 'destino 2']);
        //action
        var newState = destinos_viajes_state_model_1.reducerDestinosViajes(prevState, action);
        //assertions
        expect(newState.items.length).toEqual(2);
        expect(newState.items[0].nombre).toEqual('destino 1');
    });
    it('should reduce new item added', function () {
        var prevState = destinos_viajes_state_model_1.intializeDestinosViajesState();
        var action = new destinos_viajes_state_model_1.NuevoDestinoAction(new destino_viaje_model_1.DestinoViaje('barcelona', 'url'));
        var newState = destinos_viajes_state_model_1.reducerDestinosViajes(prevState, action);
        expect(newState.items.length).toEqual(1);
        expect(newState.items[0].nombre).toEqual('barcelona');
    });
});
